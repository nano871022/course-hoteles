$(function(){
			$("[data-toggle='tooltip']").tooltip();
			$("[data-toggle='popover']").popover();
			$('.carousel').carousel({
				interval: 5000
			});
		});
		$("#reserva").on("show.bs.modal",function (e){
			console.log("El popup de reserva se encuentra inicializando para ser mostrado");
			$(".btn-reserva").removeClass("btn-primary");
			$(".btn-reserva").prop("disable",true);
			$(".btn-reserva").css("background","#ccc");
			$(".btn-reserva").css("color","#000");
		});
		$("#reserva").on("shown.bs.modal",function (e){
			console.log("El popup de reserva se encuentra mostrandose");
		});
		$("#reserva").on("hide.bs.modal",function(e){
			console.log("El popup de reserva se encuentra inicializando para ser oculado");
		});
		$("#reserva").on("hidden.bs.modal",function (e){
			console.log("El popup reserva se encuentra oculto");
			$(".btn-reserva").addClass("btn-primary");
			$(".btn-reserva").prop("disable",false);
			$(".btn-reserva").css("background","#aeb0c2");
			$(".btn-reserva").css("color","#fff");
		});